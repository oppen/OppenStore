# ÖppenStore

I've given up on Google (in general), and the Play Store (so many reasons). All future non-commercial projects will available through this ÖppenStore Android app  

## Rules for inclusion

* Source code should be available for all projects. 
* No Google Services API allowed (all projects should run without issue on [LineageOS](https://lineageos.org/) or any other de-Googled Android fork). 
* No analytics of any kind - include an email driven feedback form if necessary.

## Catalogue Format

Main index Json holds just enough data to show the main feed. Each app/project will have a `o_store.md` file in a root directory for the product detail screen.

### Main index

Entry point to the store, the main index can list apps/PWAs, link to another index, link to a web page. A link to a web url could be used for desktop software such as the command line ÖppenHald project.

```
{
    "storeVersion": "1.0.1",
    "items": [
        {
            "type": "app",
            "title": "Lätt",
            "subtitle": "fast image filtering app for Android",
            "image": "https://oppenlab.net/services/oppenstore/assets/latt/latt_poster_image.jpg",
            "url": "https://oppenlab.net/services/oppenstore/assets/latt"
        },
        {
            "type": "index",
            "title": "Graphics Utils",
            "subtitle": "Apps and tools for working with photos",
            "url": "https://oppenlab.net/services/oppenstore/indexes/index_graphics.json"
        }
    ]
}
```

### Software Screen

This project initially used a RecyclerView to render content for the project screen with content held in a json array, while very quick it was painful to manage and to create new entries.
Instead entries should now use Markdown with some hidden metadata in the header:

```
<!-- headerimage: https://oppenlab.net/services/oppenstore/assets/latt/latt_poster_image.jpg -->
<!-- title: Lätt -->
<!-- packageName: oppen.latt.filters -->
<!-- packageUrl: https://oppenlab.net/services/oppenstore/assets/latt/app_1_0_1.apk -->
<!-- latestVersion: 1.0.1 -->

Some text etc

A description.

Usual [Markdown](...) links...

```

<hr>
************ IGNORE BELOW ************
************ CHANGES COMING **********

## Project Listing Format

* **oppen_id** - unique identifier, for Android APKs use app package name
* **title** - project title
* **type** - currently `apk` or `pwa`, `desktop_pwa` may appear in the future
* **version** - this will drive app update logic for APKs
* **web_url** - optional web page for the project
* **image** - main banner image for the project
* **url** - download URL of the APK, or web url for a PWA
* **short_description** - single line overview of the project
* **content** - an array of content blocks, current types: `text`, `image`, `web_button`


Example entry:
```json
{
    "oppen_id": "oppen.android.redaktor",
    "title": "Redaktör",
    "type": "apk",
    "version": "1.0.0",
    "web_url": "https://oppenlab.net",
    "image": "https://oppenlab.net/services/oppenstore/assets/redaktor/store_image_redaktor.png",
    "url": "https://oppenlab.net/services/oppenstore/assets/apk/opeen_redaktor_release_1_0_0.apk",
    "short_description": "A text editor designed for use with Termux and external keyboards",
    "content": [
        {
            "type": "text",
            "text": "A text editor designed for use with Termux and external keyboards, Redaktör uses Androids 'Storage Access Framework' which allows read/write access to files in device storage, Termux, Google Drive and other services. When using with a physical keyboard standard shortcuts are available (ctrl-o, ctrl-s, ctrl-n for open/save/new)."
        },
        {
            "type": "image",
            "image": "https://oppenlab.net/services/oppenstore/assets/redaktor/oppen_redactor_markdown.png"
        },
        {
            "type": "web_button",
            "url": "https://codeberg.org/oppen/Latt",
            "text": "View Lätt on Codeberg"
        }
    ]
    }
```