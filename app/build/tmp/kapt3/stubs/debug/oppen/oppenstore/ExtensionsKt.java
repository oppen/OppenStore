package oppen.oppenstore;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u001a\u0010\u0003\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0001\u001a\u0012\u0010\u0007\u001a\u00020\b*\u00020\u00022\u0006\u0010\t\u001a\u00020\u0001\u001a\u0014\u0010\n\u001a\u00020\u0004*\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0001\u00a8\u0006\u000b"}, d2 = {"appVersion", "", "Landroid/content/Context;", "installApk", "", "url", "title", "installed", "", "packageName", "showUrl", "app_debug"})
public final class ExtensionsKt {
    
    public static final boolean installed(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$installed, @org.jetbrains.annotations.NotNull()
    java.lang.String packageName) {
        return false;
    }
    
    public static final void showUrl(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$showUrl, @org.jetbrains.annotations.Nullable()
    java.lang.String url) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String appVersion(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$appVersion) {
        return null;
    }
    
    public static final void installApk(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$installApk, @org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    java.lang.String title) {
    }
}