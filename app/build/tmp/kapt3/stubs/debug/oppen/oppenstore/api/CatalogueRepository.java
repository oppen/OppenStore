package oppen.oppenstore.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004JV\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2!\u0010\u000f\u001a\u001d\u0012\u0013\u0012\u00110\u000e\u00a2\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\f0\u00102!\u0010\u0014\u001a\u001d\u0012\u0013\u0012\u00110\u0015\u00a2\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\f0\u0010H\u0002JT\u0010\u0017\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2!\u0010\u000f\u001a\u001d\u0012\u0013\u0012\u00110\u000e\u00a2\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\f0\u00102!\u0010\u0014\u001a\u001d\u0012\u0013\u0012\u00110\u0015\u00a2\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\f0\u0010JN\u0010\u0007\u001a\u00020\f2!\u0010\u000f\u001a\u001d\u0012\u0013\u0012\u00110\u000e\u00a2\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\f0\u00102#\u0010\u0014\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00010\u0006\u00a2\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0005\u0012\u0004\u0012\u00020\f0\u0010J\u0006\u0010\u0018\u001a\u00020\fR\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"}, d2 = {"Loppen/oppenstore/api/CatalogueRepository;", "", "datasource", "Loppen/oppenstore/api/CatalogueDatasource;", "(Loppen/oppenstore/api/CatalogueDatasource;)V", "apps", "Loppen/oppenstore/api/model/Apps;", "getApps", "()Loppen/oppenstore/api/model/Apps;", "setApps", "(Loppen/oppenstore/api/model/Apps;)V", "findApp", "", "oppenId", "", "onError", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "message", "callback", "Loppen/oppenstore/api/model/App;", "app", "getApp", "getStoreVersion", "app_debug"})
public final class CatalogueRepository {
    @org.jetbrains.annotations.Nullable()
    private oppen.oppenstore.api.model.Apps apps;
    private final oppen.oppenstore.api.CatalogueDatasource datasource = null;
    
    @org.jetbrains.annotations.Nullable()
    public final oppen.oppenstore.api.model.Apps getApps() {
        return null;
    }
    
    public final void setApps(@org.jetbrains.annotations.Nullable()
    oppen.oppenstore.api.model.Apps p0) {
    }
    
    public final void getStoreVersion() {
    }
    
    public final void getApps(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super oppen.oppenstore.api.model.Apps, kotlin.Unit> callback) {
    }
    
    public final void getApp(@org.jetbrains.annotations.NotNull()
    java.lang.String oppenId, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super oppen.oppenstore.api.model.App, kotlin.Unit> callback) {
    }
    
    private final void findApp(java.lang.String oppenId, kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError, kotlin.jvm.functions.Function1<? super oppen.oppenstore.api.model.App, kotlin.Unit> callback) {
    }
    
    public CatalogueRepository(@org.jetbrains.annotations.NotNull()
    oppen.oppenstore.api.CatalogueDatasource datasource) {
        super();
    }
}