package oppen.oppenstore.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004JP\u0010\u0005\u001a\u00020\u00062!\u0010\u0007\u001a\u001d\u0012\u0013\u0012\u00110\t\u00a2\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\u00060\b2#\u0010\r\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00010\u000e\u00a2\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\u000f\u0012\u0004\u0012\u00020\u00060\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Loppen/oppenstore/api/DebugCatalogue;", "Loppen/oppenstore/api/CatalogueDatasource;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getApps", "", "onError", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "message", "callback", "Loppen/oppenstore/api/model/Apps;", "apps", "app_debug"})
public final class DebugCatalogue implements oppen.oppenstore.api.CatalogueDatasource {
    private final android.content.Context context = null;
    
    @java.lang.Override()
    public void getApps(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super oppen.oppenstore.api.model.Apps, kotlin.Unit> callback) {
    }
    
    public DebugCatalogue(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}