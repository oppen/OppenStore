package oppen.oppenstore.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"Loppen/oppenstore/api/MoshiProvider;", "", "()V", "mosh", "Lcom/squareup/moshi/Moshi;", "moshi", "getMoshi", "()Lcom/squareup/moshi/Moshi;", "app_debug"})
public final class MoshiProvider {
    private static com.squareup.moshi.Moshi mosh;
    public static final oppen.oppenstore.api.MoshiProvider INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.squareup.moshi.Moshi getMoshi() {
        return null;
    }
    
    private MoshiProvider() {
        super();
    }
}