package oppen.oppenstore.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002JP\u0010\u0003\u001a\u00020\u00042!\u0010\u0005\u001a\u001d\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\u00040\u00062#\u0010\u000b\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00010\f\u00a2\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\r\u0012\u0004\u0012\u00020\u00040\u0006H\u0016\u00a8\u0006\u000e"}, d2 = {"Loppen/oppenstore/api/OppenCatalogue;", "Loppen/oppenstore/api/CatalogueDatasource;", "()V", "getApps", "", "onError", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "message", "callback", "Loppen/oppenstore/api/model/Apps;", "apps", "app_debug"})
public final class OppenCatalogue implements oppen.oppenstore.api.CatalogueDatasource {
    
    @java.lang.Override()
    public void getApps(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onError, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super oppen.oppenstore.api.model.Apps, kotlin.Unit> callback) {
    }
    
    public OppenCatalogue() {
        super();
    }
}