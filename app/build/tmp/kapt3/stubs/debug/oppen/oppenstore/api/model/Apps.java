package oppen.oppenstore.api.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\b\u0007\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u00a2\u0006\u0002\u0010\tR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\r\u00a8\u0006\u0011"}, d2 = {"Loppen/oppenstore/api/model/Apps;", "", "store_version", "", "store_repository", "store_apk", "apps", "", "Loppen/oppenstore/api/model/App;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getApps", "()Ljava/util/List;", "getStore_apk", "()Ljava/lang/String;", "getStore_repository", "getStore_version", "Companion", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class Apps {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String store_version = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String store_repository = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String store_apk = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<oppen.oppenstore.api.model.App> apps = null;
    public static final oppen.oppenstore.api.model.Apps.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStore_version() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStore_repository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStore_apk() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<oppen.oppenstore.api.model.App> getApps() {
        return null;
    }
    
    public Apps(@org.jetbrains.annotations.NotNull()
    java.lang.String store_version, @org.jetbrains.annotations.NotNull()
    java.lang.String store_repository, @org.jetbrains.annotations.NotNull()
    java.lang.String store_apk, @org.jetbrains.annotations.NotNull()
    java.util.List<oppen.oppenstore.api.model.App> apps) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\bJ\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"}, d2 = {"Loppen/oppenstore/api/model/Apps$Companion;", "", "()V", "generate", "Loppen/oppenstore/api/model/Apps;", "json", "", "response", "Lokhttp3/Response;", "bufferedSource", "Lokio/BufferedSource;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final oppen.oppenstore.api.model.Apps generate(@org.jetbrains.annotations.NotNull()
        okio.BufferedSource bufferedSource) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final oppen.oppenstore.api.model.Apps generate(@org.jetbrains.annotations.NotNull()
        java.lang.String json) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final oppen.oppenstore.api.model.Apps generate(@org.jetbrains.annotations.NotNull()
        okhttp3.Response response) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}