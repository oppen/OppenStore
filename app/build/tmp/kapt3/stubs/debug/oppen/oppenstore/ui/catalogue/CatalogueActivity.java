package oppen.oppenstore.ui.catalogue;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0016J\u0012\u0010\u000b\u001a\u00020\u00072\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0014J\u0016\u0010\u000e\u001a\u00020\u00072\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010H\u0016J\u0010\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\tH\u0016J\u0010\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\tH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Loppen/oppenstore/ui/catalogue/CatalogueActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Loppen/oppenstore/ui/catalogue/CatalogueView;", "()V", "presenter", "Loppen/oppenstore/ui/catalogue/CataloguePresenter;", "checkVersion", "", "storeVersion", "", "storeUrl", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "showApps", "apps", "", "Loppen/oppenstore/api/model/App;", "showError", "error", "storeGitRepository", "url", "app_debug"})
public final class CatalogueActivity extends androidx.appcompat.app.AppCompatActivity implements oppen.oppenstore.ui.catalogue.CatalogueView {
    private oppen.oppenstore.ui.catalogue.CataloguePresenter presenter;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void checkVersion(@org.jetbrains.annotations.NotNull()
    java.lang.String storeVersion, @org.jetbrains.annotations.NotNull()
    java.lang.String storeUrl) {
    }
    
    @java.lang.Override()
    public void storeGitRepository(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    @java.lang.Override()
    public void showApps(@org.jetbrains.annotations.NotNull()
    java.util.List<oppen.oppenstore.api.model.App> apps) {
    }
    
    @java.lang.Override()
    public void showError(@org.jetbrains.annotations.NotNull()
    java.lang.String error) {
    }
    
    public CatalogueActivity() {
        super();
    }
}