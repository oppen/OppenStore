package oppen.oppenstore.ui.catalogue;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Loppen/oppenstore/ui/catalogue/CataloguePresenter;", "", "view", "Loppen/oppenstore/ui/catalogue/CatalogueView;", "repository", "Loppen/oppenstore/api/CatalogueRepository;", "(Loppen/oppenstore/ui/catalogue/CatalogueView;Loppen/oppenstore/api/CatalogueRepository;)V", "getStore", "", "app_debug"})
public final class CataloguePresenter {
    private final oppen.oppenstore.ui.catalogue.CatalogueView view = null;
    private final oppen.oppenstore.api.CatalogueRepository repository = null;
    
    public final void getStore() {
    }
    
    public CataloguePresenter(@org.jetbrains.annotations.NotNull()
    oppen.oppenstore.ui.catalogue.CatalogueView view, @org.jetbrains.annotations.NotNull()
    oppen.oppenstore.api.CatalogueRepository repository) {
        super();
    }
}