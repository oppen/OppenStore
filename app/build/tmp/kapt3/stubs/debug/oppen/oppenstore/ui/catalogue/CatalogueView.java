package oppen.oppenstore.ui.catalogue;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H&J\u0016\u0010\u0007\u001a\u00020\u00032\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u0005H&J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0005H&\u00a8\u0006\u000f"}, d2 = {"Loppen/oppenstore/ui/catalogue/CatalogueView;", "", "checkVersion", "", "storeVersion", "", "storeUrl", "showApps", "apps", "", "Loppen/oppenstore/api/model/App;", "showError", "error", "storeGitRepository", "url", "app_debug"})
public abstract interface CatalogueView {
    
    public abstract void checkVersion(@org.jetbrains.annotations.NotNull()
    java.lang.String storeVersion, @org.jetbrains.annotations.NotNull()
    java.lang.String storeUrl);
    
    public abstract void storeGitRepository(@org.jetbrains.annotations.NotNull()
    java.lang.String url);
    
    public abstract void showApps(@org.jetbrains.annotations.NotNull()
    java.util.List<oppen.oppenstore.api.model.App> apps);
    
    public abstract void showError(@org.jetbrains.annotations.NotNull()
    java.lang.String error);
}