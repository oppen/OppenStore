package oppen.oppenstore.api

import okhttp3.OkHttpClient
import okhttp3.Request

object NetworkClient {
    private var oKClient: OkHttpClient? = null

    val client: OkHttpClient
        get() {
            if (oKClient == null) {
                oKClient = OkHttpClient.Builder()
                    .build()

            }
            return oKClient!!
        }


    fun requestBuilder(): Request.Builder {
        return Request.Builder()
            .header("Content-Type", "application/json")
    }
}