package oppen.oppenstore.api

import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import oppen.oppenstore.BuildConfig
import oppen.oppenstore.api.model.Apps
import java.io.IOException

class OppenCatalogue : CatalogueDatasource{

    override fun getApps(onError: (message: String) -> Unit, callback: (apps: Apps?) -> Unit) {

        val url = when {
            BuildConfig.DEBUG -> {
                "https://oppenlab.net/services/oppenstore/oppenstore_debug.json"
            }
            else -> {
                "https://oppenlab.net/services/oppenstore/oppenstore_live.json"
            }
        }
        NetworkClient.run {
            val request = requestBuilder()
                .url(url)
                .get()
                .build()

            client.newCall(request).enqueue(object: Callback {
                override fun onResponse(call: Call, response: Response) {
                    val appsWrapper = Apps.generate(response)
                    callback.invoke(appsWrapper)
                }

                override fun onFailure(call: Call, e: IOException) {
                    onError(e.toString())
                }
            })
        }
    }
}