package oppen.oppenstore.api.model

import com.squareup.moshi.JsonClass
import okhttp3.Response
import okio.BufferedSource
import oppen.oppenstore.api.MoshiProvider

@JsonClass(generateAdapter = true)
class Apps(
    val store_version: String,
    val store_repository: String,
    val store_apk: String,
    val apps: List<App>
){
    companion object{
        fun generate(bufferedSource: BufferedSource): Apps? {
            val responseAdapter = MoshiProvider.moshi.adapter(Apps::class.java)
            return responseAdapter.fromJson(bufferedSource)
        }

        fun generate(json: String): Apps? {
            val responseAdapter = MoshiProvider.moshi.adapter(Apps::class.java)
            return responseAdapter.fromJson(json)
        }

        fun generate(response: Response): Apps?{
            return generate(
                response.body?.string() ?: ""
            )
        }
    }
}